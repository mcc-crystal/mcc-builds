# MCC Builds

This is a repository for pre-compiled executable files of
[MCC](https://gitlab.com/mcc-crystal/mcc), created for people who lack the
knowledge to compile the program themselves. Executables provided are only
available for selected platforms and are not optimized for particular CPU
architecture.

Visit the [MCC group](https://gitlab.com/mcc-crystal) for documentation and
source code.

## Availability

Executable files are only available for 64-bit Windows. If you need executables
for another platform, consider compiling the program yourself or contact the
author.

Windows executables are compiled with
[TDM-GCC](http://tdm-gcc.tdragon.net/index.php/), which statically links
`winpthreads` in each executable. Therefore, `winpthreads` license terms
need to be bundled with the executables. They can be viewed in file
`COPYING.winpthreads.txt`. MCC itself is available under
[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Instructions

Download the executable that matches your operating system. Newest versions
are recommended. Consult
[documentation](https://gitlab.com/mcc-crystal/mcc-manual) on how to use the
program.